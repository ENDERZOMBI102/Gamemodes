@file:Suppress("UnstableApiUsage")
plugins {
    kotlin("jvm") version "1.7.20"
    id("org.quiltmc.loom") version "0.12.+"
}

repositories {
    mavenCentral()
    maven( url = "https://repsy.io/mvn/enderzombi102/mc" )
    maven( url = "https://maven.terraformersmc.com/releases" )
}

val libs = extensions.getByType<VersionCatalogsExtension>().named("libs")

/** Utility function that retrieves a bundle from the version catalog. */
fun bundle( bundleName: String ) =
	libs.findBundle( bundleName ).get()

/** Utility function that retrieves a version from the version catalog. */
fun version( key: String ) = libs.findVersion(key).get().requiredVersion

loom {
	serverOnlyMinecraftJar()
	runtimeOnlyLog4j.set(true)
}


dependencies {
	minecraft( "com.mojang:minecraft:${version("minecraft")}" )
	mappings( "org.quiltmc:quilt-mappings:${version("minecraft")}+build.${version("mappings")}:intermediary-v2" )
    implementation( bundle("implementation") )
    modImplementation( bundle("modImplementation") )
	include( bundle("include") )
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "17"
}

tasks.withType<ProcessResources> {
    inputs.property( "version"          , version )
	inputs.property( "group"            , project.group )
	inputs.property( "description"      , project.description )
    inputs.property( "loader_version"   , version("loader") )
    inputs.property( "minecraft_version", version("minecraft") )
    inputs.property( "repo_url"         , project.ext["repo_url"] )
    filteringCharset = "UTF-8"

    filesMatching( "quilt.mod.json" ) {
        expand(
            "version"      to version,
            "group"        to project.group,
            "description"  to project.description,
            "repo_url"     to project.ext["repo_url"],
            "dependencies" to """
				{ "id": "minecraft", "versions": ">=${version("minecraft")}" },
				{ "id": "java", "versions": ">=17" }""".trimIndent()
        )
        filter { it.substringBefore("///") }
    }
	filesMatching( "fabric.mod.json" ) {
		expand(
			"version"      to version,
			"description"  to project.description,
			"repo_url"     to project.ext["repo_url"],
			"dependencies" to """
				"minecraft": ">=${version("minecraft")}",
				"java": ">=17"
				""".trimIndent()
		)
		filter { it.substringBefore("///") }
	}
}

tasks.withType<Jar> {
    from( "LICENSE" ) {
        rename { "${it}_$archiveBaseName}" }
    }
}
