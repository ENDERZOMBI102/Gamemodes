package com.enderzombi102.gamemodes.imixin;

import net.minecraft.item.Item;
import org.jetbrains.annotations.NotNull;

/**
 * Interface to make ItemStacks modifiable, aka capable of changing their item type
 */
public interface ModifiableItemStack {
	void gm$setItem( @NotNull Item item );
}
