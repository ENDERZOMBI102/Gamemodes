package com.enderzombi102.gamemodes.mixin;

import com.enderzombi102.gamemodes.imixin.ModifiableItemStack;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;

@SuppressWarnings("DeprecatedIsStillUsed")
@Mixin(ItemStack.class)
public abstract class ItemStackMixin implements ModifiableItemStack {

	@Shadow
	@Final
	@Deprecated
	@Mutable
	private Item item;

	@Override
	public void gm$setItem( @NotNull Item item ) {
		this.item = item;
	}
}
