package com.enderzombi102.gamemodes.mixin.event;

import com.enderzombi102.gamemodes.event.WorldChangeEvents;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.server.world.ServerWorld;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Entity.class)
public abstract class EntityMixin {

	@Shadow public ServerWorld world;

	@Shadow public abstract BlockState getBlockStateAtPos();

	@Inject( method = "moveToWorld", at = @At("HEAD"), cancellable = true )
	public void onWorldChange( ServerWorld destination, CallbackInfoReturnable<Entity> cir ) {
		if (!
				WorldChangeEvents.ALLOW_ENTITY_WORLD_CHANGE.invoker().allowWorldChange(
						(Entity) (Object) this,
						destination,
						getBlockStateAtPos().getBlock().equals( Blocks.NETHER_PORTAL )
				)
		) cir.setReturnValue( null );
	}

}
