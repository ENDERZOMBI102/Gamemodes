package com.enderzombi102.gamemodes.mixin.event;

import com.enderzombi102.gamemodes.event.*;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.RaycastContext;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ServerPlayerEntity.class)
public abstract class ServerPlayerEntityMixin extends LivingEntity {
	@Shadow
	public abstract ServerWorld getWorld();

	protected ServerPlayerEntityMixin( EntityType<? extends LivingEntity> entityType, World world ) {
		super( entityType, world );
	}

	private BlockPos gm$lastResultPos = null;
	private BlockPos gm$lastBlockPos = null;
	private ChunkPos gm$lastChunkPos = null;
	private Vec3d gm$lastPos = null;

	/**
	 * @reason pre-tick event, raycast hit changed event
	 */
	@Inject(method = "playerTick", at = @At("HEAD"))
	public void onPreTick( CallbackInfo ci ) {
		PlayerTickEvent.PRE_TICK.invoker().onTick( (ServerPlayerEntity) (Object) this );

		var cameraPos = getEyePos();
		var cameraRotation = getRotationVecClient();
		var result = getWorld().raycast(
			new RaycastContext(
				cameraPos,
				cameraPos.add(
					cameraRotation.x * 100,
					cameraRotation.y * 100,
					cameraRotation.z * 100
				),
				RaycastContext.ShapeType.COLLIDER,
				RaycastContext.FluidHandling.NONE,
				this
			)
		);

		PlayerRaycastEvents.HIT.invoker().onRaycast(
			(ServerPlayerEntity) (Object) this,
			result.getBlockPos(),
			result.getSide()
		);

		if ( !result.getBlockPos().equals( gm$lastResultPos ) ) {
			// just get the result if its the first time
			if ( gm$lastResultPos != null )
				// block changed
				PlayerRaycastEvents.HIT_CHANGED.invoker().onChanged(
					(ServerPlayerEntity) (Object) this,
					gm$lastResultPos,
					result.getBlockPos(),
					result.getSide()
				);
			gm$lastResultPos = result.getBlockPos();
		}
	}

	/**
	 * @reason post-tick event, position changed, block position changed event, chunk position changed
	 */
	@Inject(method = "playerTick", at = @At("TAIL"))
	public void onPostTick( CallbackInfo ci ) {
		PlayerTickEvent.POST_TICK.invoker().onTick( (ServerPlayerEntity) (Object) this );

		// --- pos changed ---
		if ( !getPos().equals( gm$lastPos ) ) {
			// just get the result if it's the first time
			var result = ActionResult.PASS;
			if ( gm$lastPos != null ) {
				// block pos changed
				result = PlayerMoveEvents.PLAYER_MOVED.invoker().onMove(
					(ServerPlayerEntity) (Object) this,
					gm$lastPos,
					getPos()
				);
				if ( result == ActionResult.CONSUME )
					setPosition( gm$lastPos );
				else
					gm$lastPos = getPos();
			} else
				gm$lastPos = getPos();
		}

		// --- block pos changed ---
		if ( !getBlockPos().equals( gm$lastBlockPos ) ) {
			// just get the result if it's the first time
			var result = ActionResult.PASS;
			if ( gm$lastBlockPos != null ) {
				// block pos changed
				result = PlayerMoveEvents.PLAYER_BLOCK_MOVED.invoker().onMove(
					(ServerPlayerEntity) (Object) this,
					gm$lastBlockPos,
					getBlockPos()
				);
				if ( result == ActionResult.CONSUME )
					setPosition(
						gm$lastBlockPos.getX(),
						gm$lastBlockPos.getY(),
						gm$lastBlockPos.getZ()
					);
				else
					gm$lastBlockPos = getBlockPos();
			} else {
				gm$lastBlockPos = getBlockPos();
			}
		}

		// --- chunk pos changed ---
		if ( !getChunkPos().equals( gm$lastChunkPos ) ) {
			// just get the result if it's the first time
			if ( gm$lastChunkPos != null )
				// block changed
				PlayerChangedChunkEvent.CHUNK_CHANGED.invoker().onChanged(
					(ServerPlayerEntity) (Object) this,
					gm$lastChunkPos,
					getChunkPos()
				);
			gm$lastChunkPos = getChunkPos();
		}
	}

	@Inject(method = "moveToWorld", at = @At("HEAD"), cancellable = true)
	public void onWorldChange( ServerWorld destination, CallbackInfoReturnable<Entity> cir ) {
		if ( !
			WorldChangeEvents.ALLOW_PLAYER_WORLD_CHANGE.invoker().allowWorldChange(
				(ServerPlayerEntity) (Object) this,
				destination,
				getBlockStateAtPos().getBlock().equals( Blocks.NETHER_PORTAL )
			)
		) cir.setReturnValue( null );
	}

}
