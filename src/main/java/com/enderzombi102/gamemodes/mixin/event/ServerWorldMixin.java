package com.enderzombi102.gamemodes.mixin.event;

import com.enderzombi102.gamemodes.event.EntitySpawnEvent;
import net.minecraft.entity.Entity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.ActionResult;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ServerWorld.class)
public abstract class ServerWorldMixin {

	@Inject( method = "spawnEntity", at = @At("HEAD"), cancellable = true )
	public void onSpawnEntity( Entity entity, CallbackInfoReturnable<Boolean> cir) {
		var result = EntitySpawnEvent.ON_SPAWN.invoker().onEntitySpawn( (ServerWorld) (Object) this, entity );

		if ( result != ActionResult.PASS )
			cir.cancel();
	}


}
