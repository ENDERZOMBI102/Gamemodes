package com.enderzombi102.gamemodes;

import com.enderzombi102.gamemodes.command.ModeCommand;
import com.enderzombi102.gamemodes.util.Util;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/*
	--    implemented modes   --
		- ChunkEffect
		- BlockPainter
		- BedrockPainter
		- ExplosiveCursor
		- TeleTimer
		- Tagged (NEED TESTING)
		- TnTWorld (NEED TESTING)
		- Dropcalipse (NEED TESTING)
		- DeathSwap (NEED TESTING)
		- ManHunt (NEED TESTING)
	-- to be implemented modes --
		- Mobcalipse
		- Hide'n'Seek
		- PropHunt
 */
public class Gamemodes implements ModInitializer {

	public static final Logger LOGGER = LogManager.getLogger("Gamemodes");

	@Override
	public void onInitialize() {
		LOGGER.info("Initializing Gamemodes..");
		CommandRegistrationCallback.EVENT.register(
				(dispatcher, dedicated) -> ModeCommand.register(dispatcher)
		);

		ServerLifecycleEvents.SERVER_STARTED.register( server -> Util.SERVER = server );

		LOGGER.info("Gamemodes initialized! lets play!");
	}

}
