package com.enderzombi102.gamemodes;

import com.enderzombi102.gamemodes.mode.Mode;
import com.enderzombi102.gamemodes.mode.bedrockpainter.BedrockPainter;
import com.enderzombi102.gamemodes.mode.blockpainter.BlockPainter;
import com.enderzombi102.gamemodes.mode.chunkeffect.ChunkEffect;
import com.enderzombi102.gamemodes.mode.deathswap.DeathSwap;
import com.enderzombi102.gamemodes.mode.dropcalipse.Dropcalipse;
import com.enderzombi102.gamemodes.mode.explosivecursor.ExplosiveCursor;
import com.enderzombi102.gamemodes.mode.hidenseek.HidenSeek;
import com.enderzombi102.gamemodes.mode.manhunt.ManHunt;
import com.enderzombi102.gamemodes.mode.mobcalipse.Mobcalipse;
import com.enderzombi102.gamemodes.mode.prophunt.PropHunt;
import com.enderzombi102.gamemodes.mode.tagged.Tagged;
import com.enderzombi102.gamemodes.mode.teletimer.TeleTimer;
import com.enderzombi102.gamemodes.mode.tntworld.TntWorld;
import com.enderzombi102.gamemodes.util.Util;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.server.command.ServerCommandSource;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class ModeManager {

	private static final ArrayList<Mode> activeModes = new ArrayList<>();
	private static final HashMap< Class<? extends Mode>, Constructor<? extends Mode> > constructors = new HashMap<>();

	public static void registerGamemode( final @NotNull Class<? extends Mode> mode ) throws IllegalStateException {
		try {
			var constructor = mode.getDeclaredConstructor(CommandContext.class );
			constructors.put( mode, constructor );
		} catch (NoSuchMethodException e) {
			throw new IllegalStateException( Util.format("Mode {} doen't contain a valid constructor!", mode.getSimpleName() ), e );
		}
	}

	public static int startGamemode( final Class<? extends Mode> mode, final CommandContext<ServerCommandSource> ctx ) {
		// faster and safer route for when we already used this mode
		Constructor<? extends Mode> constructor = constructors.get(mode);

		if ( !isActive( mode.getSimpleName() ) ) {
			try {
				activeModes.add(constructor.newInstance(ctx));
			} catch (InstantiationException | IllegalAccessException e) {
				// thrown if something's up
				e.printStackTrace();
				return 1;
			} catch ( InvocationTargetException ignored ) {
				// thrown when a mode can't start
				return 1;
			}
			return 0;
		}
		return 1;
	}

	public static void stopGamemode(String name) {
		activeModes.stream()
				// find the first mode with this name
				.filter( mode -> mode.getClass().getSimpleName().equalsIgnoreCase(name) )
				.findFirst()
				// if present, stop it and remove it from the list
				.ifPresent( (mode) -> {
					mode.stop();
					activeModes.remove(mode);
				} );

	}

	@SuppressWarnings({"OptionalGetWithoutIsPresent", "unchecked"})
	public static <T extends Mode> T getMode(Class<T> clazz) {
		return (T) activeModes.stream().filter( mode -> mode.getClass() == clazz ).findFirst().get();
	}

	public static boolean isActive(String name) {
		return activeModes.stream().anyMatch(
				mode -> mode.getClass().getSimpleName().equalsIgnoreCase(name)
		);
	}

	public static boolean isActive(Class<? extends Mode> clazz) {
		return activeModes.stream().anyMatch( mode -> mode.getClass() == clazz );
	}

	public static List<String> getActiveModeNames() {
		return activeModes.stream()
				.map( mode -> mode.getClass().getSimpleName().toLowerCase() )
				.toList();
	}

	public static List< Class<? extends Mode> > getRegisteredModes() {
		return constructors.keySet().stream().toList();
	}

	public static List< String > getRegisteredModeNames() {
		return constructors.keySet()
				.stream()
				.map( Class::getSimpleName )
				.map( String::toLowerCase )
				.toList();
	}

	public static @Nullable Class<? extends Mode> getRegisteredMode( @NotNull String name ) {
		return constructors.keySet()
				.stream()
				.filter( mode -> mode.getSimpleName().equalsIgnoreCase(name) )
				.findFirst()
				.orElse(null);
	}

	static {
		registerGamemode(BedrockPainter.class);
		registerGamemode(BlockPainter.class);
		registerGamemode(ChunkEffect.class);
		registerGamemode(DeathSwap.class);
		registerGamemode(Dropcalipse.class);
		registerGamemode(ExplosiveCursor.class);
		registerGamemode(HidenSeek.class);
		registerGamemode(ManHunt.class);
		registerGamemode(Mobcalipse.class);
		registerGamemode(PropHunt.class);
		registerGamemode(Tagged.class);
		registerGamemode(TeleTimer.class);
		registerGamemode(TntWorld.class);
	}
}
