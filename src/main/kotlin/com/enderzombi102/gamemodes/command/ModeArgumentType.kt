package com.enderzombi102.gamemodes.command

import com.enderzombi102.gamemodes.ModeManager
import com.mojang.brigadier.StringReader
import com.mojang.brigadier.arguments.ArgumentType
import com.mojang.brigadier.context.CommandContext
import com.mojang.brigadier.exceptions.CommandSyntaxException
import com.mojang.brigadier.suggestion.Suggestions
import com.mojang.brigadier.suggestion.SuggestionsBuilder
import java.util.concurrent.CompletableFuture

class ModeArgumentType( private val filter: (String) -> Boolean ) : ArgumentType<String> {
	@Throws(CommandSyntaxException::class)
	override fun parse(reader: StringReader): String = reader.readString()

	override fun <S> listSuggestions( context: CommandContext<S>, builder: SuggestionsBuilder ): CompletableFuture<Suggestions> {
		for (name in ModeManager.getRegisteredModeNames().filter( filter ) )
			if (name.startsWith(builder.remainingLowerCase))
				builder.suggest(name)
		return builder.buildFuture()
	}

	override fun getExamples() = EXAMPLES

	companion object {
		private val EXAMPLES: List<String> = listOf( "ManHunt", "ChunkEffect", "DeathSwap", "PropHunt" )

		@JvmStatic
		fun modes() = ModeArgumentType { true }

		@JvmStatic
		fun activeModes() = ModeArgumentType( ModeManager::isActive )
	}
}