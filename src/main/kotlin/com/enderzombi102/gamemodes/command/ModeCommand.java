package com.enderzombi102.gamemodes.command;

import com.enderzombi102.gamemodes.ModeManager;
import com.enderzombi102.gamemodes.mode.bedrockpainter.BedrockPainter;
import com.enderzombi102.gamemodes.mode.blockpainter.BlockPainter;
import com.enderzombi102.gamemodes.mode.chunkeffect.ChunkEffect;
import com.enderzombi102.gamemodes.mode.deathswap.DeathSwap;
import com.enderzombi102.gamemodes.mode.dropcalipse.Dropcalipse;
import com.enderzombi102.gamemodes.mode.explosivecursor.ExplosiveCursor;
import com.enderzombi102.gamemodes.mode.hidenseek.HidenSeek;
import com.enderzombi102.gamemodes.mode.manhunt.ManHunt;
import com.enderzombi102.gamemodes.mode.mobcalipse.Mobcalipse;
import com.enderzombi102.gamemodes.mode.prophunt.PropHunt;
import com.enderzombi102.gamemodes.mode.tagged.Tagged;
import com.enderzombi102.gamemodes.mode.teletimer.TeleTimer;
import com.enderzombi102.gamemodes.mode.tntworld.TntWorld;
import com.enderzombi102.gamemodes.util.Const;
import com.enderzombi102.gamemodes.util.TextBuilder;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

import java.util.ArrayList;

import static com.enderzombi102.gamemodes.command.ModeArgumentType.activeModes;
import static com.enderzombi102.gamemodes.command.ModeArgumentType.modes;
import static com.mojang.brigadier.arguments.BoolArgumentType.bool;
import static com.mojang.brigadier.arguments.IntegerArgumentType.integer;
import static net.minecraft.command.argument.EntityArgumentType.players;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;


public final class ModeCommand {

	public static void register( CommandDispatcher<ServerCommandSource> dispatcher ) {
		dispatcher.register(
			literal( "mode" )
				// start a mode
				.then( literal( "start" )
					.requires( source -> source.hasPermissionLevel( 4 ) )
					// PAINTERS
					.then( literal( "bedrockpainter" )
						.executes( ctx -> ModeManager.startGamemode( BedrockPainter.class, ctx ) )
					)
					.then( literal( "blockpainter" )
						.executes( ctx -> ModeManager.startGamemode( BlockPainter.class, ctx ) )
					)
					// CHUNK EFFECT
					.then( literal( "chunkeffect" )
						.then( argument( "maxLevel", integer( 1, 255 ) )
							.executes( ctx -> ModeManager.startGamemode( ChunkEffect.class, ctx ) )
						)
						.executes( ctx -> ModeManager.startGamemode( ChunkEffect.class, ctx ) )
					)
					// DEATH SWAP
					.then( literal( "deathswap" )
						// defaults:
						//  - time: 5m
						//  - hardcore: false
						//  - allowNether: false
						.then( argument( "time", integer( 1, 255 ) )
							.then( argument( "hardcore", bool() )
								.then( argument( "allowNether", bool() )
									.executes( ctx -> ModeManager.startGamemode( DeathSwap.class, ctx ) )
								)
							)
						)
						.then( argument( "hardcore", bool() )
							.then( argument( "allowNether", bool() )
								.executes( ctx -> ModeManager.startGamemode( DeathSwap.class, ctx ) )
							)
						)
						.then( argument( "allowNether", bool() )
							.executes( ctx -> ModeManager.startGamemode( DeathSwap.class, ctx ) )
						)
						.executes( ctx -> ModeManager.startGamemode( DeathSwap.class, ctx ) )
					)
					// APOCALIPSES
					.then( literal( "dropcalipse" )
						.then( argument( "maxDrops", integer( 1, 1024 ) )
							.then( argument( "randomDrops", bool() )
								.executes( ctx -> ModeManager.startGamemode( Dropcalipse.class, ctx ) )
							)
						)
					)
					.then( literal( "mobcalispe" )
						.executes( ctx -> ModeManager.startGamemode( Mobcalipse.class, ctx ) )
					)
					// EXPLOSIVE CURSOR
					.then( literal( "explosivecursor" )
						.executes( ctx -> ModeManager.startGamemode( ExplosiveCursor.class, ctx ) )
					)
					// TAGGED
					.then( literal( "tagged" )
						.executes( ctx -> ModeManager.startGamemode( Tagged.class, ctx ) )
					)
					// TELETIMER
					.then( literal( "teletimer" )
						.then( argument( "timeBetweenTeleports", integer( 1, 255 ) )
							.then( argument( "usePearls", bool() )
								.executes( ctx -> ModeManager.startGamemode( TeleTimer.class, ctx ) )
							)
							.executes( ctx -> ModeManager.startGamemode( TeleTimer.class, ctx ) )
						)
						.then( argument( "usePearls", bool() )
							.executes( ctx -> ModeManager.startGamemode( TeleTimer.class, ctx ) )
						)
						.executes( ctx -> ModeManager.startGamemode( TeleTimer.class, ctx ) )
					)
					// WORLD
					.then( literal( "tntworld" )
						.then( argument( "blocksBetweenTnt", integer( 5, 255 ) )
							.executes( ctx -> ModeManager.startGamemode( TntWorld.class, ctx ) )
						)
						.executes( ctx -> ModeManager.startGamemode( TntWorld.class, ctx ) )
					)
					// HUNT
					.then( literal( "manhunt" )
						.then( argument( "deathSpectator", bool() )
							.then( argument( "giveCompassOnRespawn", bool() )  // default: false
								.then( argument( "huntedPlayers", players() )
									.executes( ctx -> ModeManager.startGamemode( ManHunt.class, ctx ) )
								)
							)
							.then( argument( "huntedPlayers", players() )
								.executes( ctx -> ModeManager.startGamemode( ManHunt.class, ctx ) )
							)
						)
					)
					.then( literal( "prophunt" )
						.executes( ctx -> ModeManager.startGamemode( PropHunt.class, ctx ) )
					)
					.then( literal( "hide'n'seek" )
						.executes( ctx -> ModeManager.startGamemode( HidenSeek.class, ctx ) )
					)
				)
				// stop a mode
				.then( literal( "stop" )
					.requires( source -> source.hasPermissionLevel( 4 ) )
					.then( argument( "mode", activeModes() )
						.executes( ModeCommand::stopMode )
					)
				)
				// list active modes
				.then( literal( "list" )
					.executes( ModeCommand::listActive )
				)
				// send info about the mod
				.then( literal( "info" )
					.executes( ctx -> {
						var txt = Text.literal(
							"Gamemodes by ENDERZOMBI102\nServer-side mod for minigames and gamemodes\nVersion: " + Const.MOD_VERSION
						);
						txt.fillStyle( Style.EMPTY.withColor( Formatting.BLUE ) );
						ctx.getSource().sendFeedback( txt, false );
						return 0;
					} )
				)
				.then( literal( "help" )
					.then( argument( "mode", modes() )
						.executes( ctx -> {
							var modeName = ctx.getArgument( "mode", String.class );

							final TextBuilder help = new TextBuilder( "Showing help for \"" + modeName + "\"", Formatting.YELLOW );
							help.resetStyle();
							switch ( modeName ) {
								case "bedrockpainter" -> {
									help.addLine( "BedrockPainter is a mode where everything you point with your cursor becomes bedrock" );
									help.addLine( "Arguments:", Formatting.DARK_BLUE );
									help.addParsed( " <clr:yellow>-<clr:white> N/D" );
								}
								case "blockpainter" -> {
									help.addLine( "BlockPainter is a mode where everything you point with your cursor transforms into a random block" );
									help.addLine( "Arguments:", Formatting.DARK_BLUE );
									help.addParsed( " <clr:yellow>-<clr:white> N/D" );
								}
								case "chunkeffect" -> {
									help.addLine( "ChunkEffect is a mode where every chunk has a different effect on you" );
									help.addLine( "Arguments:", Formatting.DARK_BLUE );
									help.addParsed( " - <clr:blue>maxLevel<clr:white>: an integer between 1 and 255 (inclusive), 100 by default, the max level the enchants can have" );
								}
								case "deathswap" -> {
									help.addLine( "DeathSwap is a mode where every player is trying to kill another player, without weapons or damaging him" );
									help.addLine( "Arguments:", Formatting.DARK_BLUE );
									help.addParsed( " - <clr:blue>time<clr:white>: integer between 1 and 255 (inclusive), 5 by default, time between swaps" );
									help.addParsed( " - <clr:blue>hardcore<clr:white>: boolean, false by default, if true, players can respawn and continue to play" );
									help.addParsed( " - <clr:blue>allowNether<clr:white>: boolean, false by default, if true, allows going to the nether" );
								}
								case "dropcalipse" -> {
									help.addLine( "Dropcalipse is a mode where drop quantity is randomized (items too if enabled)" );
									help.addLine( "Arguments:", Formatting.DARK_BLUE );
									help.addParsed( " - <clr:blue>maxDrops<clr:white>: an integer between 1 and 1024 (inclusive), max quantity of drops from single source" );
									help.addParsed( " - <clr:blue>randomDrops<clr:white>: boolean, whether drop items are randomized" );
								}
								case "explodingcursor" -> {
									help.addLine( "ExplodingCursor is a mode where if you point at a block, it'll explode" );
									help.addLine( "Arguments:", Formatting.DARK_BLUE );
									help.addParsed( " <clr:yellow>-<clr:white> N/D" );
								}
								case "hidenseek" -> {
									help.addLine( "Hide'n'Seek is a mode where there's a seeker that seeks other players, played in adventure mode" );
									help.addLine( "Arguments:", Formatting.DARK_BLUE );
									help.addParsed( " <clr:yellow>-<clr:white> N/D" );
								}
								case "manhunt" -> {
									help.addLine( "ManHunt is a mode where one or more players are hunted by all the other players." );
									help.addLine( "hunters can respawn, and have a compass that points to the hunted players." );
									help.addLine( "hunted players can't respawn." );
									help.addLine( "Arguments:", Formatting.DARK_BLUE );
									help.addParsed( " - <clr:blue>deathSpectator<clr:white>: boolean, put dead players into spectator mode" );
									help.addParsed( " - <clr:blue>giveCompassOnRespawn<clr:white>: boolean, false by default, gives a new compass to respawning hunters" );
									help.addParsed( " - <clr:blue>huntedPlayers<clr:white>: player names, all the players that will be hunted" );
								}
								case "mobcalipse" -> {
									help.addLine( "Mobcalipse is a mode where all mobs are more powerful and geared" );
									help.addLine( "Arguments:", Formatting.DARK_BLUE );
									help.addParsed( " <clr:yellow>-<clr:white> N/D" );
								}
								case "prophunt" -> {
									// TODO: ADD PROPHUNT DESCRIPTION + ARGUMENTS
									help.addLine( "PropHunt is a mode where " );
									help.addLine( "Arguments:", Formatting.DARK_BLUE );
									help.addParsed( " <clr:yellow>-<clr:white> N/D" );
								}
								case "tagged" -> {
									help.addLine( "Tagged is a mode where every you have to tag other players, tagged players will be put on spectator" );
									help.addLine( "Arguments:", Formatting.DARK_BLUE );
									help.addParsed( " <clr:yellow>-<clr:white> N/D" );
								}
								case "teletimer" -> {
									help.addLine( "TeleTimer is a mode where after an amout of time, you'll be teleported where your cursor is pointing at" );
									help.addLine( "Arguments:", Formatting.DARK_BLUE );
									help.addParsed( " - <clr:blue>timeBetweenTeleports<clr:white>: an integer between 1 and 255, 2 by default, " );
									help.addParsed( " - <clr:blue>usePearls<clr:white>: boolean, false by default, intead of using line of sight to teleport, use ender pearls" );
								}
								case "tntworld" -> {
									help.addLine( "TntWorld is a mode where every time you move 5 (or more) blocks, a TNT spawns." );
									help.addLine( "Arguments:", Formatting.DARK_BLUE );
									help.addParsed( " - <clr:blue>blocksBetweenTnt<clr:white>: an integer between 5 and 255, 5 by default, how many blocks between spawns." );
								}
								default -> help.addLine( "NOT IMPLEMENTED" );
							}

							ctx.getSource().sendFeedback( help.toText(), false );
							return 0;
						} )
					)
					.executes( ctx -> {
						ctx.getSource().sendError( Text.literal( "Usage: /mode help MODENAME " ) );
						return 0;
					} )
				)

		);
	}

	private static int stopMode( CommandContext<ServerCommandSource> ctx ) {
		ModeManager.stopGamemode(
			ctx.getArgument( "mode", String.class )
		);
		return 0;
	}

	private static int listActive( CommandContext<ServerCommandSource> ctx ) throws CommandSyntaxException {
		// construct message
		ArrayList<String> msg = new ArrayList<>() {{
			add( "** active gamemodes **" );
			for ( var mode : ModeManager.getActiveModeNames() )
				add( " - " + mode );
		}};
		if ( msg.size() == 1 ) {
			msg.add( " - no modes are active" );
		}
		// send it
		ctx.getSource().sendFeedback( Text.literal( String.join( "\n", msg ) ), false );
		return 0;
	}
}
