package com.enderzombi102.gamemodes.event

import net.fabricmc.fabric.api.event.Event
import net.fabricmc.fabric.api.event.EventFactory
import net.minecraft.entity.Entity
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.ActionResult

/**
 * Called when an entity spawns, can prevent spawning
 */
fun interface EntitySpawnEvent {
	fun onEntitySpawn( world: ServerWorld, entity: Entity ): ActionResult

	companion object {
		@JvmField
		val ON_SPAWN: Event<EntitySpawnEvent> = EventFactory.createArrayBacked( EntitySpawnEvent::class.java ) { listeners ->
			EntitySpawnEvent { world, entity ->
				for (listener in listeners) {
					val res = listener.onEntitySpawn(world, entity)
					if (res != ActionResult.PASS)
						return@EntitySpawnEvent res
				}
				ActionResult.PASS
			}
		}
	}
}