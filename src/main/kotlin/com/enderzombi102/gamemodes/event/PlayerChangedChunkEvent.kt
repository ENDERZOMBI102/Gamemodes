package com.enderzombi102.gamemodes.event

import net.fabricmc.fabric.api.event.Event
import net.fabricmc.fabric.api.event.EventFactory
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.util.math.ChunkPos

/**
 * Called when a player changes chunk position
 */
fun interface PlayerChangedChunkEvent {
	fun onChanged( player: ServerPlayerEntity, from: ChunkPos, to: ChunkPos )

	companion object {
		@JvmField
		val CHUNK_CHANGED: Event<PlayerChangedChunkEvent> = EventFactory.createArrayBacked( PlayerChangedChunkEvent::class.java ) { listeners ->
			PlayerChangedChunkEvent { player, from, to ->
				for (listener in listeners)
					listener.onChanged( player, from, to )
			}
		}
	}
}