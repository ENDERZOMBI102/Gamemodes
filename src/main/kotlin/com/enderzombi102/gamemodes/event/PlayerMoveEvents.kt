package com.enderzombi102.gamemodes.event

import com.enderzombi102.gamemodes.event.PlayerMoveEvents.PlayerBlockMoveEvent
import com.enderzombi102.gamemodes.event.PlayerMoveEvents.PlayerMoveEvent
import net.fabricmc.fabric.api.event.Event
import net.fabricmc.fabric.api.event.EventFactory
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.util.ActionResult
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3d

/**
 * Events for player movement
 */
object PlayerMoveEvents {
	/**
	 * Fired when a player pos changes.
	 * Cancelled when [ActionResult.CONSUME] is retuned
	 */
	@JvmField
	val PLAYER_MOVED: Event<PlayerMoveEvent> = EventFactory.createArrayBacked( PlayerMoveEvent::class.java ) { listeners ->
		PlayerMoveEvent { player, from, to ->
			for (listener in listeners) {
				val res = listener.onMove(player, from, to)
				if (res != ActionResult.PASS)
					return@PlayerMoveEvent res
			}
			ActionResult.PASS
		}
	}

	/**
	 * Fired when a player block pos changes (moves from one block to another).
	 * Cancelled when [ActionResult.CONSUME] is returned
	 */
	@JvmField
	val PLAYER_BLOCK_MOVED: Event<PlayerBlockMoveEvent> = EventFactory.createArrayBacked( PlayerBlockMoveEvent::class.java ) { listeners ->
		PlayerBlockMoveEvent { player, from, to ->
			for (listener in listeners) {
				val res = listener.onMove(player, from, to)
				if (res != ActionResult.PASS)
					return@PlayerBlockMoveEvent res
			}
			ActionResult.PASS
		}
	}

	fun interface PlayerMoveEvent {
		fun onMove(player: ServerPlayerEntity, from: Vec3d, to: Vec3d): ActionResult
	}

	fun interface PlayerBlockMoveEvent {
		fun onMove(player: ServerPlayerEntity, from: BlockPos, to: BlockPos): ActionResult
	}
}