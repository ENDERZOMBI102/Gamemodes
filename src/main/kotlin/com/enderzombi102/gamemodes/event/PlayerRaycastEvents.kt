package com.enderzombi102.gamemodes.event

import com.enderzombi102.gamemodes.event.PlayerRaycastEvents.PlayerRaycastHitChangedEvent
import com.enderzombi102.gamemodes.event.PlayerRaycastEvents.PlayerRaycastHitEvent
import net.fabricmc.fabric.api.event.Event
import net.fabricmc.fabric.api.event.EventFactory
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Direction

/**
 * Events for player raycasting, called every tick
 */
object PlayerRaycastEvents {
	/**
	 * Called when a player looks a different block from last tick (position must change for this to be triggered)
	 */
	@JvmField
	val HIT_CHANGED: Event<PlayerRaycastHitChangedEvent> = EventFactory.createArrayBacked( PlayerRaycastHitChangedEvent::class.java ) { listeners ->
		PlayerRaycastHitChangedEvent { player, from, to, side ->
			for (listener in listeners)
				listener.onChanged( player, from, to, side )
		}
	}

	/**
	 * Called whenever the player looks, at a max 100 of distance
	 */
	@JvmField
	val HIT: Event<PlayerRaycastHitEvent> = EventFactory.createArrayBacked( PlayerRaycastHitEvent::class.java ) { listeners ->
		PlayerRaycastHitEvent { player, pos, side ->
			for (listener in listeners)
				listener.onRaycast( player, pos, side )
		}
	}

	fun interface PlayerRaycastHitChangedEvent {
		fun onChanged(player: ServerPlayerEntity, from: BlockPos, to: BlockPos, side: Direction)
	}

	fun interface PlayerRaycastHitEvent {
		fun onRaycast(player: ServerPlayerEntity, pos: BlockPos, side: Direction)
	}
}