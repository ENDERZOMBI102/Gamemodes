package com.enderzombi102.gamemodes.event

import net.fabricmc.fabric.api.event.Event
import net.fabricmc.fabric.api.event.EventFactory
import net.minecraft.server.network.ServerPlayerEntity

fun interface PlayerTickEvent {
	fun onTick( player: ServerPlayerEntity )

	companion object {
		/**
		 * Called before player tick
		 */
		@JvmField
		val PRE_TICK: Event<PlayerTickEvent> = EventFactory.createArrayBacked( PlayerTickEvent::class.java ) { listeners ->
			PlayerTickEvent { player ->
				for (listener in listeners)
					listener.onTick( player )
			}
		}

		/**
		 * Called after player tick
		 */
		@JvmField
		val POST_TICK: Event<PlayerTickEvent> = EventFactory.createArrayBacked( PlayerTickEvent::class.java ) { listeners ->
			PlayerTickEvent { player ->
				for (listener in listeners)
					listener.onTick( player )
			}
		}
	}
}