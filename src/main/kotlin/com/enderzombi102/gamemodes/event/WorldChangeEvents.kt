package com.enderzombi102.gamemodes.event

import com.enderzombi102.gamemodes.event.WorldChangeEvents.EntityWorldChange
import com.enderzombi102.gamemodes.event.WorldChangeEvents.PlayerWorldChange
import net.fabricmc.fabric.api.event.Event
import net.fabricmc.fabric.api.event.EventFactory
import net.minecraft.entity.Entity
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld

object WorldChangeEvents {
	/**
	 * Callback for when an entity changes world (ex. overworld -> nether)
	 */
	@JvmField
	val ALLOW_ENTITY_WORLD_CHANGE: Event<EntityWorldChange> = EventFactory.createArrayBacked( EntityWorldChange::class.java ) { listeners ->
		EntityWorldChange { entity, destination, fromPortal ->
			for (listener in listeners) {
				val res = listener.allowWorldChange(entity, destination, fromPortal)
				if (!res)
					return@EntityWorldChange false
			}
			true
		}
	}

	/**
	 * Callback for when a player changes world (ex. overworld -> nether)
	 */
	@JvmField
	val ALLOW_PLAYER_WORLD_CHANGE: Event<PlayerWorldChange> = EventFactory.createArrayBacked( PlayerWorldChange::class.java ) { listeners ->
		PlayerWorldChange { entity, destination, fromPortal ->
			for (listener in listeners) {
				val res = listener.allowWorldChange(entity, destination, fromPortal)
				if (!res)
					return@PlayerWorldChange false
			}
			true
		}
	}

	fun interface EntityWorldChange {
		fun allowWorldChange(entity: Entity, destination: ServerWorld, fromPortal: Boolean): Boolean
	}

	fun interface PlayerWorldChange {
		fun allowWorldChange(player: ServerPlayerEntity, destination: ServerWorld, fromPortal: Boolean): Boolean
	}
}