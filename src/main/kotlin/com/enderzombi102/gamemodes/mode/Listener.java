package com.enderzombi102.gamemodes.mode;

public abstract class Listener {

	private boolean hasBeenRegistered = false;
	public void register() {
		if (! hasBeenRegistered ) {
			onRegister();
			hasBeenRegistered = true;
		}
	}

	protected abstract void onRegister();
	protected abstract void onStop();
}
