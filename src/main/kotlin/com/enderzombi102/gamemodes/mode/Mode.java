package com.enderzombi102.gamemodes.mode;

import com.enderzombi102.gamemodes.util.Util;
import net.minecraft.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Mode {
	protected final Logger LOGGER = LoggerFactory.getLogger( "Gamemodes [" + this.getClass().getSimpleName() + "]" );

	public abstract void stop();

	protected void broadcastPrefixedMessage( Text msg ) {
		Util.broadcastMessage( Text.literal( "[" + this.getClass().getSimpleName() + "] " ).append( msg ) );
	}

	protected void broadcastPrefixedMessage( String msg ) {
		broadcastPrefixedMessage( Text.literal( msg ) );
	}
}
