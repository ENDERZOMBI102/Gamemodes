package com.enderzombi102.gamemodes.mode.bedrockpainter;

import com.enderzombi102.gamemodes.mode.Mode;
import com.mojang.brigadier.context.CommandContext;

public class BedrockPainter extends Mode {

	public BedrockPainter(CommandContext<ServerCommandSource> ctx) {
		BedrockPainterListener.INSTANCE.register();
		broadcastPrefixedMessage( "Started!" );
	}

	@Override
	public void stop() {
		broadcastPrefixedMessage("Stopped!");
	}
}
