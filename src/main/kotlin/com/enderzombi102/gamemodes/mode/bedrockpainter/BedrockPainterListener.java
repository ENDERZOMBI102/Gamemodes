package com.enderzombi102.gamemodes.mode.bedrockpainter;

import com.enderzombi102.gamemodes.ModeManager;
import com.enderzombi102.gamemodes.event.PlayerRaycastEvents;
import com.enderzombi102.gamemodes.mode.Listener;
import net.minecraft.block.Blocks;
import net.minecraft.class_2246;
import net.minecraft.class_2338;
import net.minecraft.class_2350;
import net.minecraft.class_3218;
import net.minecraft.class_3222;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

class BedrockPainterListener extends Listener implements PlayerRaycastEvents.PlayerRaycastHitChangedEvent {
	static final BedrockPainterListener INSTANCE = new BedrockPainterListener();

	@Override
	public void onRegister() {
		PlayerRaycastEvents.HIT_CHANGED.register( INSTANCE );
	}

	@Override
	public void onChanged( ServerPlayerEntity player, BlockPos from, BlockPos to, Direction side ) {
		if ( !ModeManager.isActive( BedrockPainter.class ) )
			return;
		var world = player.getWorld();
		var state = world.getBlockState( to );
		if ( state.isAir() || !( state.hasBlockEntity() || state.isOf( Blocks.BEDROCK ) ) )
			world.setBlockState( to, Blocks.BEDROCK.getDefaultState() );
	}

	@Override
	protected void onStop() {
	}
}
