package com.enderzombi102.gamemodes.mode.blockpainter;

import com.enderzombi102.gamemodes.mode.Mode;
import com.mojang.brigadier.context.CommandContext;

public class BlockPainter extends Mode {

	public BlockPainter(CommandContext<ServerCommandSource> ctx) {
		BlockPainterListener.INSTANCE.register();
		broadcastPrefixedMessage( "Started!" );
	}

	@Override
	public void stop() {
		broadcastPrefixedMessage("Stopped!");
	}
}
