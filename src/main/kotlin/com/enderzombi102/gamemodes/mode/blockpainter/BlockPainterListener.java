package com.enderzombi102.gamemodes.mode.blockpainter;

import com.enderzombi102.gamemodes.ModeManager;
import com.enderzombi102.gamemodes.event.PlayerRaycastEvents;
import com.enderzombi102.gamemodes.mode.Listener;
import com.enderzombi102.gamemodes.util.RandomUtil;
import com.enderzombi102.gamemodes.util.Util;
import net.minecraft.class_2248;
import net.minecraft.class_2338;
import net.minecraft.class_2350;
import net.minecraft.class_3218;
import net.minecraft.class_3222;

class BlockPainterListener extends Listener implements PlayerRaycastEvents.PlayerRaycastHitChangedEvent {
	static final BlockPainterListener INSTANCE = new BlockPainterListener();

	@Override
	public void onRegister() {
		PlayerRaycastEvents.HIT_CHANGED.register(INSTANCE);
	}

	@Override
	public void onChanged(class_3222 player, class_2338 from, class_2338 to, class_2350 side) {
		if (! ModeManager.isActive(BlockPainter.class) )
			return;
		class_3218 world = player.method_14220();
		if (
				world.method_8320(to).method_26215() ||
				!world.method_8320(to).method_31709()
		) {
			class_2248 block;
			do {
				block = RandomUtil.randomEntry( Util.SAFE_BLOCKS );
			} while ( world.method_8320(to).method_26204() == block );
			world.method_8501( to, block.method_9564() );
		}
	}

	@Override
	protected void onStop() { }
}
