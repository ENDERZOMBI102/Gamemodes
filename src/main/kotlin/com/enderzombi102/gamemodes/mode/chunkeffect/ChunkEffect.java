package com.enderzombi102.gamemodes.mode.chunkeffect;

import com.enderzombi102.gamemodes.mode.Mode;
import com.enderzombi102.gamemodes.util.Util;
import com.mojang.brigadier.context.CommandContext;

public class ChunkEffect extends Mode {

	final int maxLevel;

	public ChunkEffect(CommandContext<ServerCommandSource> ctx) {
		maxLevel = Util.getArgument(
				ctx,
				"maxLevel",
				Integer.class,
				100
		);
		ChunkEffectListener.INSTANCE.register();
		broadcastPrefixedMessage( "Started!" );
	}

	@Override
	public void stop() {
		ChunkEffectListener.INSTANCE.onStop();
		broadcastPrefixedMessage("Stopped!");
	}
}
