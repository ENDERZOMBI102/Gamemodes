package com.enderzombi102.gamemodes.mode.chunkeffect;

import com.enderzombi102.gamemodes.ModeManager;
import com.enderzombi102.gamemodes.event.PlayerChangedChunkEvent;
import com.enderzombi102.gamemodes.mode.Listener;
import com.enderzombi102.gamemodes.util.RandomUtil;
import com.enderzombi102.gamemodes.util.Util;
import java.util.HashMap;
import net.minecraft.class_1293;
import net.minecraft.class_1923;
import net.minecraft.class_3222;

class ChunkEffectListener extends Listener implements PlayerChangedChunkEvent {
	static final ChunkEffectListener INSTANCE = new ChunkEffectListener();
	final HashMap<class_1923, class_1293> chunkEffects = new HashMap<>();

	@Override
	public void onRegister() {
		PlayerChangedChunkEvent.CHUNK_CHANGED.register(INSTANCE);
	}

	@Override
	public void onChanged(final class_3222 player, final class_1923 from, final class_1923 to) {
		if (! ModeManager.isActive(ChunkEffect.class) )
			return;

		// in case we don't have an effect for the next chunk, add it
		if (! chunkEffects.containsKey( to ) ) {
			chunkEffects.put(
					to,
					new class_1293(
							RandomUtil.randomEntry( Util.LASTING_EFFECTS ), // type
							Integer.MAX_VALUE, // duration
							RandomUtil.randomInt( ModeManager.getMode(ChunkEffect.class).maxLevel ), // level
							false, // particles
							false // particles 2
					)
			);
		}

		// remove the old effect if present
		if ( chunkEffects.containsKey( from ) )
			player.method_6016(
					chunkEffects.get( from ).method_5579()
			);
		// apply the new one
		player.method_6092( chunkEffects.get( to ) );
	}

	public void onStop() {
		for ( var player : Util.SERVER.method_3760().method_14571() ) {
			player.removeStatusEffect( chunkEffects.get( player.getChunkPos() ).getEffectType() );
		}
		chunkEffects.clear();
	}
}
