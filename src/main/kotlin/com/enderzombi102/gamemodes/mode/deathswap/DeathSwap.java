package com.enderzombi102.gamemodes.mode.deathswap;

import com.enderzombi102.gamemodes.mode.Mode;
import com.enderzombi102.gamemodes.util.ClassTypeFilter;
import com.enderzombi102.gamemodes.util.Util;
import com.mojang.brigadier.context.CommandContext;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Timer;
import net.minecraft.class_1267;
import net.minecraft.class_1542;
import net.minecraft.class_1934;
import net.minecraft.class_2338;
import net.minecraft.class_2561;
import net.minecraft.class_3218;
import net.minecraft.class_3222;
import net.minecraft.class_3545;
import var;

import static com.enderzombi102.gamemodes.util.Util.ONE_SECOND;

public class DeathSwap extends Mode {

	final boolean allowNether;
	final boolean hardcore;
	private final Timer timerThread;
	private final ArrayList< class_3545<class_3222, class_3222> > teams = new ArrayList<>();

	public DeathSwap(CommandContext<ServerCommandSource> ctx) {
		// add event listener for this gamemode
		DeathSwapListener.INSTANCE.register();
		// send mode info
		broadcastPrefixedMessage("Setting up DeathSwap..");
		// get arguments
		int swapTime = Util.getArgument( ctx, "time", Integer.class, 5 );
		int time = swapTime * 60;
		broadcastPrefixedMessage("timer: " + time / 60 + " minutes");
		// make the teams
		broadcastPrefixedMessage("making teams");
		this.makeTeams();
		// set world
		broadcastPrefixedMessage("setting up world");
		this.hardcore = Util.getArgument( ctx, "hardcore", Boolean.class, false );
		this.allowNether = Util.getArgument( ctx, "allowNether", Boolean.class, false );
		setupWorld(hardcore);
		// start the timer thread
		this.timerThread = new Timer();
		this.timerThread.scheduleAtFixedRate( new DeathSwapTimer( this, time ), ONE_SECOND, ONE_SECOND );
		broadcastPrefixedMessage("the clock is ticking! timer started!");
	}

	private void makeTeams() {
		ArrayList<class_3222> players = new ArrayList<>( Util.getPlayers() );
		// remove all players that aren't in survival mode
		players.removeIf( player -> player.field_13974.method_14257() != class_1934.field_9215 );
		// determine how many teams will exist
		int teamCount = players.size() / 2, playerIndex = 0;
		broadcastPrefixedMessage("and now the antagonists!");
		for ( int i = 0; i < teamCount; i++ ) {
			var pair = new class_3545<>(
					players.get( playerIndex++ ),
					players.get( playerIndex++ )
			);
			this.teams.add( pair );
			broadcastPrefixedMessage( pair.getLeft().getName() + " vs " + pair.getRight().getName() );
		}
	}


	public void swap() {
		// cycle in the teams
		this.teams.forEach(team -> {
			// swap!
			team.method_15442().method_7353( class_2561.method_30163( "swap!" ), false );
			team.method_15441().method_7353( class_2561.method_30163( "swap!" ), false );
			class_2338 p1 = team.method_15442().method_24515();
			class_3218 p1world = team.method_15442().method_14220();
			class_2338 p2 = team.method_15441().method_24515();
			class_3218 p2world = team.method_15441().method_14220();
			team.method_15441().method_14251(
					p1world,
					p1.method_10263(),
					p1.method_10264(),
					p1.method_10260(),
					team.method_15441().method_36454(),
					team.method_15441().method_36455()
			);
			team.method_15442().method_14251(
					p2world,
					p2.method_10263(),
					p2.method_10264(),
					p2.method_10260(),
					team.method_15442().method_36454(),
					team.method_15442().method_36455()
			);
		});
	}

	/**
	 * Returns true when handled
	 */
	public boolean checkWin( class_3222 nowDeadPlayer ) {
		for ( var team : this.teams ) {

			if ( team.getLeft().equals( nowDeadPlayer ) ) {
				broadcastPrefixedMessage(team.getRight().getName() + " Wins his SwapBattle!");
				team.getLeft().changeGameMode( class_1934.field_9219 );
				return true;
			} else if ( team.getRight().equals( nowDeadPlayer ) ) {
				broadcastPrefixedMessage(team.getLeft().getName() + " Wins his SwapBattle!");
				team.getRight().changeGameMode( class_1934.field_9219 );
				return true;
			}
		}
		return false;
	}

	private static void setupWorld( boolean hardcore ) {
		Util.SERVER.method_3776( class_1267.field_5801, true );
		Util.SERVER.method_3776( hardcore ? class_1267.field_5807 : class_1267.field_5802, true );
		// remove all monsters
		for ( class_3218 world : Util.SERVER.method_3738() ) {
			Util.killMonsters(world);
			Util.killEntities(
					world,
					new ClassTypeFilter<>(class_1542.class),
					Objects::nonNull
			);
		}
		// reset spawn location and teleport to world spawn
		class_2338 spawn = Util.SERVER.method_30002().method_27911();
		for ( class_3222 player : Util.SERVER.method_3760().method_14571() ) {
			// only change for players in survival
			if ( player.field_13974.method_14257() != class_1934.field_9215 )
				continue;
			player.method_31548().method_5448();
			player.method_14251(
					Util.SERVER.method_30002(),
					spawn.method_10263(),
					spawn.method_10264(),
					spawn.method_10260(),
					player.method_36454(),
					player.method_36455()
			);
		}
	}

	@Override
	public void stop() {
		this.timerThread.cancel();
	}
}
