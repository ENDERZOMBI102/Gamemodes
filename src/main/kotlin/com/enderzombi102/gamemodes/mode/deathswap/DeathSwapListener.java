package com.enderzombi102.gamemodes.mode.deathswap;

import com.enderzombi102.gamemodes.ModeManager;
import com.enderzombi102.gamemodes.event.WorldChangeEvents;
import com.enderzombi102.gamemodes.mode.Listener;
import net.fabricmc.fabric.api.entity.event.v1.ServerPlayerEvents;
import net.minecraft.class_2561;

class DeathSwapListener extends Listener {
	static final DeathSwapListener INSTANCE = new DeathSwapListener();

	@Override
	protected void onRegister() {
		ServerPlayerEvents.ALLOW_DEATH.register(
				(attacked, source, amount) -> {
					if (! ModeManager.isActive(DeathSwap.class) )
						return true;

					return !ModeManager.getMode(DeathSwap.class).checkWin(attacked);
				}
		);
		WorldChangeEvents.ALLOW_PLAYER_WORLD_CHANGE.register(
				(player, destination, fromPortal) -> {
					if (! ModeManager.isActive(DeathSwap.class) )
						return true;
					if (! fromPortal )
						return true;

					if (! ModeManager.getMode(DeathSwap.class).allowNether ) {
						player.method_7353(
								class_2561.method_30163(
										"Travelling to the nether has been disabled by the rules"
								),
								false
						);
						return false;
					}
					return true;
				}
		);
	}


	@Override
	protected void onStop() {

	}
}
