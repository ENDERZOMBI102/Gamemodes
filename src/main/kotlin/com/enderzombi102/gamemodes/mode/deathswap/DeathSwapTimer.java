package com.enderzombi102.gamemodes.mode.deathswap;

import com.enderzombi102.gamemodes.util.Util;

import java.util.TimerTask;

class DeathSwapTimer extends TimerTask {

	private final int time;
	private int counter;
	private final DeathSwap deathSwap;

	public DeathSwapTimer(DeathSwap deathSwap, int time) {
		this.time = time;
		this.counter = time;
		this.deathSwap = deathSwap;
	}

	@Override
	public void run() {
		// What you want to schedule goes here
		if ( counter <= 10  && counter != 0) {
			Util.broadcastMessage("Swapping in " + counter--);
		} else if ( counter == 0 ) {
			this.counter = this.time;
			deathSwap.swap();
		} else {
			counter--;
		}
	}
}
