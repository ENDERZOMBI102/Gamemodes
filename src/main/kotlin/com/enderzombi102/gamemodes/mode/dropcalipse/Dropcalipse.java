package com.enderzombi102.gamemodes.mode.dropcalipse;

import com.enderzombi102.gamemodes.mode.Mode;
import com.mojang.brigadier.context.CommandContext;
import java.util.Random;
import net.minecraft.class_1792;
import net.minecraft.class_1799;
import net.minecraft.class_2378;

public class Dropcalipse extends Mode {
	static final Random RANDOM = new Random();
	final int maxDrops;
	final boolean randomizeDrops;


	public Dropcalipse(CommandContext<ServerCommandSource> ctx) {
		maxDrops = ctx.getArgument("maxDrops", Integer.class);
		randomizeDrops = ctx.getArgument("randomDrops", Boolean.class);
		DropcalipseListener.INSTANCE.register();
		broadcastPrefixedMessage("The Dropcalipse is started! good luck!");
	}

	@Override
	public void stop() {

	}

	public class_1799 randomize(class_1799 stack) {

		class_1792 item = stack.method_7909();
		int amount;
		do {
			amount = RANDOM.nextInt( this.maxDrops );
		} while( amount < 4 );
		if ( this.randomizeDrops ) {
			do {
				item = class_2378.field_11142.method_10240( RANDOM );
			} while( item == stack.method_7909() );
		}

		return new class_1799( item, amount );
	}
}
