package com.enderzombi102.gamemodes.mode.dropcalipse;

import com.enderzombi102.gamemodes.ModeManager;
import com.enderzombi102.gamemodes.mode.Listener;
import xyz.nucleoid.stimuli.Stimuli;
import xyz.nucleoid.stimuli.event.block.BlockDropItemsEvent;
import xyz.nucleoid.stimuli.event.entity.EntityDropItemsEvent;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.class_1269;
import net.minecraft.class_1271;
import net.minecraft.class_1799;
import var;

class DropcalipseListener extends Listener {
	static final DropcalipseListener INSTANCE = new DropcalipseListener();

	@Override
	protected void onRegister() {
		Stimuli.global().listen(
				BlockDropItemsEvent.EVENT,
				( breaker, world, pos, state, dropStacks ) -> this.processDrops( dropStacks )
		);
		Stimuli.global().listen(
				EntityDropItemsEvent.EVENT,
				( dropper, items ) -> this.processDrops( items )
		);
	}

	private class_1271< List< class_1799 > > processDrops( List<class_1799> items ) {
		if (! ModeManager.isActive(Dropcalipse.class) )
			return new class_1271<>( class_1269.field_5811, items );

		var newDrops = new ArrayList<class_1799>();

		for ( class_1799 stack : items )
			newDrops.add( ModeManager.getMode( Dropcalipse.class ).randomize( stack ) );

		return new class_1271<>( class_1269.field_5812, newDrops );
	}

	@Override
	protected void onStop() {

	}
}
