package com.enderzombi102.gamemodes.mode.explosivecursor;

import com.enderzombi102.gamemodes.mode.Mode;
import com.mojang.brigadier.context.CommandContext;

public class ExplosiveCursor extends Mode {

	public ExplosiveCursor(CommandContext<ServerCommandSource> ctx) {
		ExplosiveCursorListener.INSTANCE.register();
	}

	@Override
	public void stop() {
		ExplosiveCursorListener.INSTANCE.onStop();
		broadcastPrefixedMessage("Stopped!");
	}
}
