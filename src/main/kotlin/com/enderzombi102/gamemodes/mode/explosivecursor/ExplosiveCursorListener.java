package com.enderzombi102.gamemodes.mode.explosivecursor;

import com.enderzombi102.gamemodes.ModeManager;
import com.enderzombi102.gamemodes.event.PlayerRaycastEvents;
import com.enderzombi102.gamemodes.mode.Listener;
import com.enderzombi102.gamemodes.util.Cooldown;
import java.util.HashMap;
import java.util.UUID;
import net.minecraft.class_1927;
import net.minecraft.class_2338;
import net.minecraft.class_2350;
import net.minecraft.class_3222;

class ExplosiveCursorListener extends Listener implements PlayerRaycastEvents.PlayerRaycastHitEvent {
	static final ExplosiveCursorListener INSTANCE = new ExplosiveCursorListener();
	final HashMap<UUID, Cooldown> COOLDOWNS = new HashMap<>();

	@Override
	public void onRegister() {
		PlayerRaycastEvents.HIT.register(INSTANCE);
	}

	@Override
	public void onRaycast(final class_3222 player, class_2338 pos, class_2350 side) {
		if (! ModeManager.isActive(ExplosiveCursor.class) )
			return;

		// missing cooldown?
		if (! COOLDOWNS.containsKey( player.method_5667() ) )
			COOLDOWNS.put( player.method_5667(), new Cooldown(10) );

		if ( COOLDOWNS.get( player.method_5667() ).tick() ) {
			player.method_14220().method_8437(
					null,
					pos.method_10263(),
					pos.method_10264(),
					pos.method_10260(),
					10,
					class_1927.class_4179.field_18686
			);
		}
	}

	@Override
	protected void onStop() {
		COOLDOWNS.clear();
	}

}
