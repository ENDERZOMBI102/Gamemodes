package com.enderzombi102.gamemodes.mode.manhunt;

import java.util.UUID;
import net.minecraft.class_1282;
import net.minecraft.class_1934;
import net.minecraft.class_2338;

public class Hunted extends ManHuntPlayer {
	private static final long LOCATION_UPDATE_TIME = 400; // millis

	public class_2338 lastLocation;
	public long lastLocationUpdate = 0;

	public Hunted(UUID handle) {
		super(handle);
	}

	public boolean isAlive() {
		return getPlayer().field_13974.method_14257() == class_1934.field_9215;
	}

	@Override
	public void onRespawn() { }

	@Override
	public void onDamage(class_1282 source, float damage) {
		// "kill" the hunted
		getPlayer().method_6033(20);
		getPlayer().method_7346();
		getPlayer().method_31548().method_5448();
		getPlayer().method_7336( class_1934.field_9219 );

		// if there is more hunted players alive, target another hunted player
		if (! manHunt.allPreyDead() ) {
			// cycle in the hunters
			for ( Hunter hunter : manHunt.hunters ) {
				// update only the hunters who targeted this player
				if ( this == hunter.getPrey() ) {
					// update their target to the first hunted player that is not this
					hunter.setPrey( manHunt.getAlivePreys().get(0) );
				}
			}
		}
		// check if every damaged is dead
		manHunt.checkFinish();
	}

	@Override
	public void onMove(class_2338 pos) {
		if ( System.currentTimeMillis() - lastLocationUpdate > LOCATION_UPDATE_TIME ) {
			lastLocation = pos;
			lastLocationUpdate = System.currentTimeMillis();
		}
	}

	@Override
	public boolean isHunter() {
		return false;
	}
}
