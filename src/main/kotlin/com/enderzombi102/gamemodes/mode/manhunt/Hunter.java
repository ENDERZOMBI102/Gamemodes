package com.enderzombi102.gamemodes.mode.manhunt;

import com.enderzombi102.gamemodes.util.IterableInventory;
import java.util.UUID;
import net.minecraft.class_1282;
import net.minecraft.class_1799;
import net.minecraft.class_1802;
import net.minecraft.class_2338;
import net.minecraft.class_2512;
import net.minecraft.class_2561;
import var;

public class Hunter extends ManHuntPlayer {

	private Hunted prey;

	public Hunter(UUID handle, Hunted prey) {
		super(handle);
		this.prey = prey;
		giveCompass();
	}

	public void setPrey(Hunted player) {
		this.prey = player;
	}

	public Hunted getPrey() {
		return prey;
	}

	@Override
	public void onMove(class_2338 pos) {
		// tick compass
		for ( class_1799 stack : new IterableInventory( getPlayer().method_31548() ) ) {
			// its a compass?
			if ( stack.method_7909() != class_1802.field_8251 )
				continue;
			// its the tracker compass?
			if (! stack.method_7948().method_10545("TrackingCompass") )
				continue;

			// update it
			var nbt = stack.method_7948();
			nbt.putBoolean("LodestoneTracked", false);
			nbt.putString(
					"LodestoneDimension",
					prey.getPlayer()
							.method_14220()
							.method_27983()
							.method_29177()
							.toString()
			);
			nbt.put("LodestonePos", class_2512.method_10692( prey.lastLocation ) );
			stack.method_7980( nbt );
		}
	}

	@Override
	public boolean isHunter() {
		return true;
	}

	@Override
	public void onRespawn() {
		if (manHunt.giveCompassOnRespawn) {
			// on respawn, give the hunter another compass
			this.giveCompass();
		}
	}

	@Override
	public void onDamage(class_1282 source, float damage) { }

	public void giveCompass() {
		// get the stack
		class_1799 stack = new class_1799(class_1802.field_8251);
		var nbt = stack.method_7948();
		nbt.putBoolean("TrackingCompass", true);
		nbt.putBoolean("LodestoneTracked", false);
		nbt.putString(
				"LodestoneDimension",
				prey.getPlayer()
						.method_14220()
						.method_27983()
						.method_29177()
						.toString()
		);
		nbt.put("LodestonePos", class_2512.method_10692( prey.lastLocation ) );
		stack.method_7980( nbt );
		stack.method_7977( class_2561.method_30163("Tracking Compass") );
		this.getPlayer().method_31548().method_7394( stack );
	}

	public void nextPrey() {
		var index = manHunt.preys.indexOf( prey ) + 1;
		prey = manHunt.preys.get( index > manHunt.preys.size() ? 0 : index );
	}
}
