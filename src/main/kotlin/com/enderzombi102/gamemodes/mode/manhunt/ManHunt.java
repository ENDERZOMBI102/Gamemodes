package com.enderzombi102.gamemodes.mode.manhunt;

import com.enderzombi102.gamemodes.ModeManager;
import com.enderzombi102.gamemodes.mode.Mode;
import com.enderzombi102.gamemodes.util.Util;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import net.minecraft.class_1934;
import net.minecraft.class_2186;
import net.minecraft.class_2338;
import net.minecraft.class_3222;

public class ManHunt extends Mode {

	final boolean deathSpectator;
	final boolean giveCompassOnRespawn;
	private final class_2338 startPoint;
	final List<Hunter> hunters = new ArrayList<>();
	final List<Hunted> preys;

	@SuppressWarnings("StringConcatenationInsideStringBufferAppend")
	public ManHunt(CommandContext<ServerCommandSource> ctx) throws CommandSyntaxException {
		broadcastPrefixedMessage("Starting manhunt!");
		// arguments
		deathSpectator = ctx.getArgument( "deathSpectator", Boolean.class );
		giveCompassOnRespawn = Util.getArgument(
				ctx,
				"giveCompassOnRespawn",
				Boolean.class,
				false
		);
		this.preys = class_2186
				.method_9312( ctx, "huntedPlayers" )
				.stream()
				.map( player -> new Hunted( player.method_5667() ) )
				.toList();

		// setup attributes
		this.startPoint = ctx.getSource().method_9207().method_24515();
		// start listeners
		ManHuntListener.INSTANCE.register();
		// give every player an initial target and the Tracking Compass
		for ( class_3222 player : Util.getPlayers() ) {
			if (! isHunted(player) ) {
				hunters.add(
					new Hunter(
							player.method_5667(),
							preys.get(0)
					)
				);
			}
		}

		// build message
		StringBuilder builder = new StringBuilder("ManHunt started! good luck to ");
		builder.append( preys.get(0).getPlayer().method_5477() );
		for (int i = 1; i < preys.size(); i++ ) {
			if ( i == preys.size() - 1 ) {
				builder.append( " and " + preys.get(i).getPlayer().method_5477() + "!" );
			} else {
				builder.append( ", " + preys.get(i).getPlayer().method_5477() );
			}
		}
		broadcastPrefixedMessage( builder.toString() );
	}

	@Override
	public void stop() {
		// clear inventories, spawnpoints and location
		for ( class_3222 player : Util.getPlayers() ) {
			player.method_26284(
					Util.SERVER.method_30002().method_27983(),
					startPoint,
					0,
					true,
					false
			);
			player.method_31548().method_5448();
			player.method_7336( class_1934.field_9215 );
			player.method_14251(
					Util.SERVER.method_30002(),
					startPoint.method_10263(),
					startPoint.method_10264(),
					startPoint.method_10260(),
					0,
					0
			);
		}
	}

	public boolean isHunted( class_3222 player ) {
		return preys.stream().anyMatch(hunted -> hunted.getPlayer() == player );
	}

	public boolean allPreyDead() {
		return preys.stream().noneMatch( Hunted::isAlive );
	}

	public List<Hunted> getAlivePreys() {
		return preys.stream().filter( Hunted::isAlive ).toList();
	}

	public void checkFinish() {
		if ( allPreyDead() ) {
			// if reaches here, we're finished
			broadcastPrefixedMessage( "Hunters won!" );
			ModeManager.stopGamemode( "ManHunt" );
		}
	}

	public List<ManHuntPlayer> getPlayers() {
		List<ManHuntPlayer> buf = new ArrayList<>( hunters );
		buf.addAll(preys);
		return buf;
	}

	public @Nullable ManHuntPlayer getManhuntPlayer(UUID uuid) {
		return getPlayers().stream()
				.filter( player -> player.getPlayerUuid().equals(uuid) )
				.findFirst()
				.orElse(null);
	}

	public @Nullable ManHuntPlayer getManhuntPlayer( class_3222 player ) {
		return getManhuntPlayer( player.method_5667() );
	}
}
