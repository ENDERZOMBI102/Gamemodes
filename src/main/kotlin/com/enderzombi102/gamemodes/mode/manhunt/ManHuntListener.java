package com.enderzombi102.gamemodes.mode.manhunt;

import com.enderzombi102.gamemodes.ModeManager;
import com.enderzombi102.gamemodes.event.PlayerMoveEvents;
import com.enderzombi102.gamemodes.mode.Listener;
import net.fabricmc.fabric.api.entity.event.v1.ServerPlayerEvents;
import net.fabricmc.fabric.api.event.player.UseItemCallback;
import net.minecraft.class_1269;
import net.minecraft.class_1271;
import net.minecraft.class_3222;
import var;

public class ManHuntListener extends Listener {
	public static final ManHuntListener INSTANCE = new ManHuntListener();

	@Override
	protected void onRegister() {
		PlayerMoveEvents.PLAYER_BLOCK_MOVED.register( ( player, from, to ) -> {
			if (! ModeManager.isActive(ManHunt.class) )
				return class_1269.field_5811;

			var mhPlayer = ModeManager.getMode(ManHunt.class).getManhuntPlayer( player );
			if ( mhPlayer != null )
				mhPlayer.onMove(from);

			return class_1269.field_5811;
		});
		ServerPlayerEvents.ALLOW_DEATH.register( (player, damageSource, damageAmount) -> {
			if (! ModeManager.isActive(ManHunt.class) )
				return true;

			var mhPlayer = ModeManager.getMode(ManHunt.class).getManhuntPlayer( player );
			if ( mhPlayer == null ) {
				return true;
			}

			mhPlayer.onDamage(damageSource, damageAmount);
			return mhPlayer.isHunter();
		});
		ServerPlayerEvents.AFTER_RESPAWN.register( (oldPlayer, newPlayer, alive) -> {
			if (! ModeManager.isActive(ManHunt.class) )
				return;

			var mhPlayer = ModeManager.getMode(ManHunt.class).getManhuntPlayer( newPlayer );
			if ( mhPlayer != null ) {
				mhPlayer.onRespawn();
			}
		});
		UseItemCallback.EVENT.register( (player, world, hand) -> {
			if (
					(! ModeManager.isActive(ManHunt.class) ) ||
					world.method_8608() ||
					(! player.method_5998(hand).method_7948().method_10545("TrackingCompass") )
			)
				return new class_1271<>( class_1269.field_5811, player.method_5998(hand) );

			var sPlayer = (class_3222) player;

			var mhPlayer = ModeManager.getMode(ManHunt.class).getManhuntPlayer( sPlayer );
			if ( mhPlayer instanceof Hunter hunter ) {
				hunter.nextPrey();
			}

			return new class_1271<>( class_1269.field_5811, player.method_5998(hand) );
		});
	}

	@Override
	protected void onStop() {

	}
}
