package com.enderzombi102.gamemodes.mode.manhunt;

import com.enderzombi102.gamemodes.ModeManager;
import com.enderzombi102.gamemodes.util.Util;
import java.util.UUID;
import net.minecraft.class_1282;
import net.minecraft.class_2338;
import net.minecraft.class_3222;

public abstract class ManHuntPlayer {
	static final ManHuntPlayer EMPTY_VESSEL = new ManHuntPlayer(null) {
		@Override
		public void onRespawn() {}

		@Override
		public void onDamage(class_1282 source, float damage) {}

		@Override
		public void onMove(class_2338 pos) {}

		@Override
		public boolean isHunter() { return false; }
	};


	public final UUID handle;
	private class_3222 cachedEntity;
	protected final ManHunt manHunt = ModeManager.getMode(ManHunt.class);

	protected ManHuntPlayer(UUID handle) {
		this.handle = handle;
		cachedEntity = Util.SERVER.method_3760().method_14602(this.handle);
	}

	public class_3222 getPlayer() {
		// update entity if lost
		if ( cachedEntity.method_31481() )
			cachedEntity = Util.SERVER.method_3760().method_14602(this.handle);
		return cachedEntity;
	}

	public UUID getPlayerUuid() {
		return handle;
	}


	public abstract void onRespawn();
	public abstract void onDamage(class_1282 source, float damage);
	public abstract void onMove(class_2338 pos);
	public abstract boolean isHunter();
}