package com.enderzombi102.gamemodes.mode.mobcalipse;

import com.enderzombi102.gamemodes.ModeManager;
import com.enderzombi102.gamemodes.mode.Mode;
import com.enderzombi102.gamemodes.util.Util;
import com.mojang.brigadier.context.CommandContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import net.minecraft.class_1267;
import net.minecraft.class_1928;
import net.minecraft.class_3218;

public class Mobcalipse extends Mode {
	private static final Logger LOGGER = LogManager.getLogger("[MOBCALIPSE]");

	private class_1267 oldDifficulty;
	private boolean oldDoLightCycle;

	public Mobcalipse(CommandContext<ServerCommandSource> ctx) {

	}

	@Override
	public void stop() {
		Util.SERVER.method_3776( oldDifficulty, true );
		Util.SERVER
				.method_27728()
				.method_146()
				.method_20746(class_1928.field_19396)
				.method_20758( oldDoLightCycle, Util.SERVER );
		for ( class_3218 world : Util.SERVER.method_3738() ) {
			world.method_8450().method_20746( class_1928.field_19396 ).method_20758( oldDoLightCycle, Util.SERVER );
			LOGGER.info("Restored previous world values" );
		}
	}

	public void setupWorld() {
		// TODO: Finish this
		oldDifficulty = Util.SERVER.method_27728().method_207();
		oldDoLightCycle = Util.SERVER.method_27728()
				.method_27859()
				.method_146()
				.method_8355(class_1928.field_19396);
		LOGGER.info("Saved previously used values:");
		LOGGER.info(" - difficulty: " + oldDifficulty.name() );
		LOGGER.info(" - doLightCycle: " + oldDoLightCycle );
		Util.SERVER.method_3776( class_1267.field_5807, true );
		for ( class_3218 world : Util.SERVER.method_3738() ) {
			Util.killMonsters(world);
			world.method_29199(18000);
			world.method_8450().method_20746( class_1928.field_19396 ).method_20758( false, Util.SERVER );
		}
		LOGGER.info("Updated all worlds");
	}

	static Mobcalipse getInstance() {
		return ModeManager.getMode( Mobcalipse.class );
	}
}
