package com.enderzombi102.gamemodes.mode.tagged;

import com.enderzombi102.gamemodes.mode.Mode;
import com.enderzombi102.gamemodes.mode.ModeException;
import com.enderzombi102.gamemodes.util.Const;
import com.enderzombi102.gamemodes.util.Util;
import com.google.common.collect.*;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import java.util.*;
import java.util.stream.Collectors;
import net.minecraft.class_2561;

public class Tagged extends Mode {

	public Tagged(CommandContext<ServerCommandSource> ctx) throws ModeException {
		if ( Util.getPlayers().size() <= 1 && (! Const.DEBUG ) ) {
			ctx.getSource().method_9213( class_2561.method_30163("Can't start tagged if there's only one player") );
			throw new ModeException();
		}
		broadcastPrefixedMessage("Starting Tagged!");
		TaggedListener.INSTANCE.register();

		for ( var player : Util.getPlayers() ) {
			Util.setMaxHealth( player, 1 );
		}

		broadcastPrefixedMessage("Tagged started!");
		broadcastPrefixedMessage("Hit another player with the empty hand to kill him and gain more health!");
	}

	@Override
	public void stop() {
		TaggedListener.INSTANCE.onStop();
		broadcastPrefixedMessage("Stopped!");
	}
}
