package com.enderzombi102.gamemodes.mode.tagged;

import com.enderzombi102.gamemodes.Gamemodes;
import com.enderzombi102.gamemodes.ModeManager;
import com.enderzombi102.gamemodes.mode.Listener;
import com.enderzombi102.gamemodes.util.Util;
import net.fabricmc.fabric.api.event.player.AttackEntityCallback;
import net.minecraft.class_1268;
import net.minecraft.class_1269;
import net.minecraft.class_1297;
import net.minecraft.class_1657;
import net.minecraft.class_1934;
import net.minecraft.class_1937;
import net.minecraft.class_2585;
import net.minecraft.class_3222;
import net.minecraft.class_3966;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.enderzombi102.gamemodes.util.Util.broadcastMessage;

class TaggedListener extends Listener implements AttackEntityCallback {
	static final TaggedListener INSTANCE = new TaggedListener();
	final List<UUID> alivePlayers = Util.SERVER.method_3760()
			.method_14571()
			.stream()
			.map( class_1297::method_5667 )
			.collect( Collectors.toList() );
	final ArrayList<UUID> deadPlayers = new ArrayList<>();

	@Override
	protected void onRegister() {
		AttackEntityCallback.EVENT.register( this );
	}

	@SuppressWarnings("ConstantConditions")
	@Override
	protected void onStop() {
		for ( UUID uuid : deadPlayers ) {
			try {
				Util.SERVER.method_3760().method_14602(uuid).method_7336(class_1934.field_9215);
			} catch (NullPointerException e) {
				Gamemodes.LOGGER.error("A playing player isn't playing anymore? wtf?", e);
			}
		}
	}

	@Override
	public class_1269 interact(class_1657 attacker, class_1937 world, class_1268 hand, class_1297 entity, @Nullable class_3966 hitResult) {
		if ( ModeManager.isActive(Tagged.class) && entity instanceof class_3222 attacked ) {
			if ( alivePlayers.contains( attacked.method_5667() ) ) {
				// kill it
				attacked.method_7336( class_1934.field_9219 );
				attacked.method_7353(
						new class_2585( "Tagged by: " ).method_10852( attacker.method_5476() ),
						false
				);
				Util.setMaxHealth( (class_3222) attacker, (int) attacker.method_6063() + 1 );
				checkWin();
				return class_1269.field_5812;
			}
		}
		return class_1269.field_5811;
	}

	public void checkWin() {
		// win check
		if ( alivePlayers.size() > 1 )
			return;

		// scoreboard
		StringBuilder builder = new StringBuilder();
		builder.append(" - ")
				.append( Util.getPlayer( alivePlayers.get(0) ).method_5476().method_10851() )
				.append(" tagged ")
				.append( (int) ( Util.getPlayer( alivePlayers.get(0) ).method_6063() - 1 ) * 10 )
				.append(" players\n");
		// winner
		int max = (int) ( Util.getPlayer( alivePlayers.get(0) ).method_6063() - 1 ) * 10;
		String winner = Util.getPlayer( alivePlayers.get(0) ).method_5476().method_10851();

		// check who got the most tags
		for (UUID playerUuid : deadPlayers ) {
			class_3222 player = Util.getPlayer( playerUuid );
			if ( ( player.method_6063() - 1 ) * 10 > max ) {
				max = (int) ( player.method_6063() - 1 ) * 10;
				winner = player.method_5476().method_10851();
			}
			builder.append(" - ")
					.append( player.method_5476().method_10851() )
					.append(" tagged ")
					.append( (int) (player.method_6063() - 1) * 10 )
					.append(" players\n");
		}

		broadcastMessage(" --- Game finished! --- ");
		broadcastMessage("Winner: " + winner);
		broadcastMessage(" --- Scoreboard ---\n" + builder );
		ModeManager.stopGamemode("Tagged");
	}
}
