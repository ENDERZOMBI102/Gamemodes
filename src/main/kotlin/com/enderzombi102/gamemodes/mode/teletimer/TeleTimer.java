package com.enderzombi102.gamemodes.mode.teletimer;

import com.enderzombi102.gamemodes.mode.Mode;
import com.enderzombi102.gamemodes.util.Util;
import com.mojang.brigadier.context.CommandContext;
import java.util.Timer;
import net.minecraft.class_1684;
import net.minecraft.class_3222;
import var;

import static com.enderzombi102.gamemodes.util.Util.ONE_SECOND;

public class TeleTimer extends Mode {

	private final Timer timerThread;
	private final boolean usePearls;

	public TeleTimer(CommandContext<ServerCommandSource> ctx) {
		broadcastPrefixedMessage("setting up TeleTimer mode");
		usePearls = Util.getArgument(
				ctx,
				"usePearls",
				Boolean.class,
				false
		);
		int teleTime = Util.getArgument(
				ctx,
				"timeBetweenTeleports",
				Integer.class,
				2
		);
		// timer
		final int time = teleTime * 60;
		broadcastPrefixedMessage("timer: " + time / 60 + ( teleTime == 1 ? " minute" : " minutes" ) );
		timerThread = new Timer( "TeleTimerThread", false );
		timerThread.scheduleAtFixedRate( new TeleTimerTimer(time), ONE_SECOND, ONE_SECOND );
		broadcastPrefixedMessage("Started!");
	}

	@Override
	public void stop() {
		timerThread.cancel();
		broadcastPrefixedMessage("Stopped!");
	}

	public void teleport() {
		broadcastPrefixedMessage( "Teleport!" );
		for ( final class_3222 player : Util.SERVER.method_3760().method_14571() ) {
			if (this.usePearls) {
				var enderPerl = new class_1684( player.method_14220(), player );
				enderPerl.setProperties(player, player.method_36455(), player.method_36454(), 0.0F, 1.5F, 1.0F);
				player.method_14220().method_8649(enderPerl);
			} else {
				var pos = Util.rayCast( player, 200 ).method_17777();
				var yaw = player.method_36454();
				var pitch = player.method_36455();
				player.method_20620( pos.getX(), pos.up().getY(), pos.getZ() );
				player.method_36457(pitch);
				player.method_36456(yaw);
			}
		}
	}
}
