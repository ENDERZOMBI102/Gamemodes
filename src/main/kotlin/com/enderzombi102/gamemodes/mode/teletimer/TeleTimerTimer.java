package com.enderzombi102.gamemodes.mode.teletimer;

import com.enderzombi102.gamemodes.ModeManager;
import com.enderzombi102.gamemodes.util.Util;

import java.util.TimerTask;

class TeleTimerTimer extends TimerTask {

	private final int time;
	private int counter;

	public TeleTimerTimer(int time) {
		this.time = time;
		this.counter = time;
	}

	@Override
	public void run() {
		// What you want to schedule goes here
		if ( counter <= 3  && counter != 0) {
			Util.broadcastMessage("Teleport in " + counter--);
		} else if ( counter == 0 ) {
			this.counter = this.time;
			ModeManager.getMode(TeleTimer.class).teleport();
		} else {
			counter--;
		}
	}
}
