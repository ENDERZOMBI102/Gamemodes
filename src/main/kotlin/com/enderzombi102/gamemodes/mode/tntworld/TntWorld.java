package com.enderzombi102.gamemodes.mode.tntworld;

import com.enderzombi102.gamemodes.mode.Mode;
import com.enderzombi102.gamemodes.util.Util;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.server.command.ServerCommandSource;

public class TntWorld extends Mode {

	final int delay;

	public TntWorld(CommandContext<ServerCommandSource> ctx) {
		TntWorldListener.INSTANCE.register();
		delay = Util.getArgument(
				ctx,
				"blocksBetweenTnt",
				Integer.class,
				5
		);
		broadcastPrefixedMessage("Started!");
	}

	@Override
	public void stop() {
		TntWorldListener.INSTANCE.onStop();
		broadcastPrefixedMessage("Stopped!");
	}
}
