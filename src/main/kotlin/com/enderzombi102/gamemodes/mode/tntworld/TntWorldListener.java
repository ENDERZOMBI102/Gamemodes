package com.enderzombi102.gamemodes.mode.tntworld;

import com.enderzombi102.gamemodes.ModeManager;
import com.enderzombi102.gamemodes.event.PlayerMoveEvents;
import com.enderzombi102.gamemodes.mode.Listener;
import com.enderzombi102.gamemodes.mode.explosivecursor.ExplosiveCursor;
import java.util.HashMap;
import java.util.UUID;
import net.minecraft.class_1269;
import net.minecraft.class_1541;

class TntWorldListener extends Listener {
	static final TntWorldListener INSTANCE = new TntWorldListener();
	final HashMap<UUID, Integer> playerTntCountdown = new HashMap<>();

	@Override
	protected void onRegister() {
		PlayerMoveEvents.PLAYER_BLOCK_MOVED.register( ( player, from, to) -> {

			if (
					(! ModeManager.isActive(TntWorld.class) ) ||
					( from.method_10263() == to.method_10263() && from.method_10260() == to.method_10260() )
			) {
				return class_1269.field_5811;
			}

			// check if the player is present in the hashmap
			if ( playerTntCountdown.containsKey( player.method_5667() ) ) {
				// it is, do corrisponding action
				if ( playerTntCountdown.get( player.method_5667() ) > ModeManager.getMode( TntWorld.class ).delay ) {
					class_1541 tnt = new class_1541(
							player.method_14220(),
							from.method_10263(),
							from.method_10264(),
							from.method_10260(),
							player
					);
					player.method_14220().method_8649( tnt );
					playerTntCountdown.put( player.method_5667(), 0 );
				} else {
					playerTntCountdown.put(
							player.method_5667(),
							playerTntCountdown.get( player.method_5667() ) + 1
					);
				}
			} else {
				// its not, add it
				playerTntCountdown.put( player.method_5667(), 0 );
			}
			return class_1269.field_5811;
		} );
	}

	@Override
	protected void onStop() {
		playerTntCountdown.clear();
	}
}
