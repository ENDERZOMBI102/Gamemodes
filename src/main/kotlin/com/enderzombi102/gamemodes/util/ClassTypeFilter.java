package com.enderzombi102.gamemodes.util;

import net.minecraft.entity.Entity;
import net.minecraft.util.TypeFilter;
import org.jetbrains.annotations.Nullable;

public class ClassTypeFilter<T extends Entity> implements TypeFilter<Entity, T> {
	private final Class<T> clazz;

	public ClassTypeFilter(Class<T> clazz) {
		this.clazz = clazz;
	}

	@Nullable
	@Override
	public T downcast( Entity obj ) {
		return clazz.isInstance(obj) ? clazz.cast(obj) : null;
	}

	@Override
	public Class<? extends Entity> getBaseClass() {
		return clazz;
	}
}
