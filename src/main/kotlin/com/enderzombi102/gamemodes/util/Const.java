package com.enderzombi102.gamemodes.util;

import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.metadata.ModMetadata;

public final class Const {
	private Const() {}

	public static final boolean DEBUG = true;
	public static final String MOD_NAME = "Gamemodes";
	public static final String MOD_ID = "gamemodes";
	public static final String MOD_VERSION = getMetadata()
			.getVersion()
			.getFriendlyString();

	private static ModContainer getContainer() {
		return FabricLoader.getInstance()
				.getModContainer(MOD_ID)
				.orElseThrow();
	}

	private static ModMetadata getMetadata() {
		return getContainer().getMetadata();
	}



}
