package com.enderzombi102.gamemodes.util;

public class Cooldown {

	private long cooldown;
	private final long max;

	public Cooldown(long max) {
		this.max = max;
		cooldown = max;
	}

	/**
	 * Tick this Cooldown, if zero is reached, will reset and return true
	 * @return true when reached max val
	 */
	public boolean tick() {
		if ( cooldown == 0 ) {
			cooldown = max;
			return true;
		} else {
			cooldown--;
			return false;
		}
	}

}
