package com.enderzombi102.gamemodes.util;

import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.NoSuchElementException;
import net.minecraft.class_1263;
import net.minecraft.class_1799;

public class IterableInventory implements Iterator<class_1799>, Iterable<class_1799> {

	private final class_1263 inventory;
	private int position = -1;

	public IterableInventory(class_1263 inventory) {
		this.inventory = inventory;
	}


	/**
	 * Returns {@code true} if the iteration has more elements.
	 * (In other words, returns {@code true} if {@link #next} would
	 * return an element rather than throwing an exception.)
	 *
	 * @return {@code true} if the iteration has more elements
	 */
	@Override
	public boolean hasNext() {
		return inventory.method_5439() > position;
	}

	/**
	 * Returns the next element in the iteration.
	 *
	 * @return the next element in the iteration
	 * @throws NoSuchElementException if the iteration has no more elements
	 */
	@Override
	public class_1799 next() {
		position++;
		if ( position < inventory.method_5439() ) {
			return inventory.method_5438( position );
		} else {
			throw new NoSuchElementException();
		}
	}

	/**
	 * Removes from the underlying collection the last element returned
	 * by this iterator (optional operation).  This method can be called
	 * only once per call to {@link #next}.
	 * <p>
	 * The behavior of an iterator is unspecified if the underlying collection
	 * is modified while the iteration is in progress in any way other than by
	 * calling this method, unless an overriding class has specified a
	 * concurrent modification policy.
	 * <p>
	 * The behavior of an iterator is unspecified if this method is called
	 * after a call to the {@link #forEachRemaining forEachRemaining} method.
	 *
	 * @throws UnsupportedOperationException if the {@code remove}
	 *                                       operation is not supported by this iterator
	 * @throws IllegalStateException         if the {@code next} method has not
	 *                                       yet been called, or the {@code remove} method has already
	 *                                       been called after the last call to the {@code next}
	 *                                       method
	 * @implSpec The default implementation throws an instance of
	 * {@link UnsupportedOperationException} and performs no other action.
	 */
	@Override
	public void remove() {
		inventory.method_5441( position - 1 );
	}

	// ----------- Iterable -----------

	/**
	 * Returns an iterator over elements of type {@code T}.
	 *
	 * @return an Iterator.
	 */
	@NotNull
	@Override
	public Iterator<class_1799> iterator() {
		return this;
	}
}
