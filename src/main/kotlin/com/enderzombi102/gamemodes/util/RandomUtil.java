package com.enderzombi102.gamemodes.util;

import org.jetbrains.annotations.Contract;

import java.util.List;
import java.util.Random;

public class RandomUtil {

	private static java.util.Random random =  new java.util.Random();

	public static boolean possibility(int percent) {
		return random.nextFloat() <= percent / 100F;
	}

	public static boolean possibility(float percent) {
		return random.nextFloat() <= percent;
	}

	public static int randomInt(int max, int min) {
		int x;
		do {
			x = random.nextInt(max);
		} while(x < min);
		return x;
	}

	/**
	 * A thin wrapper around Random.nextInt(max)
	 * @param max the max value the returned int can be
	 * @return a random integer under max
	 */
	public static int randomInt(int max) {
		return random.nextInt(max);
	}

	/**
	 * Returns a random entry from the provided list
	 * @param list list of elements to choose from
	 * @param <E> type of element
	 * @return random element
	 */
	@Contract( pure = true )
	public static <E> E randomEntry(List<E> list) {
		return list.get( random.nextInt( list.size() - 1) );
	}

	/**
	 * Returns a random entry from the provided array
	 * @param array array of elements to choose from
	 * @param <E> type of element
	 * @return random element
	 */
	@Contract( pure = true )
	public static <E> E randomEntry(E[] array) {
		return array[ random.nextInt( array.length - 1) ];
	}

	/**
	 * Replaces the current random instance with a new one
	 */
	public static void regenerateRandomizer() {
		random = new java.util.Random();
	}

	/**
	 * Getter for the current random instance
	 * @return the random instance
	 */
	public static Random getRandom() {
		return random;
	}
}
