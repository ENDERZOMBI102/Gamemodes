package com.enderzombi102.gamemodes.util;

import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

public final class TextBuilder {

	private MutableText buf;

	public TextBuilder( String initial, Formatting fmt ) {
		buf = Text.literal( initial + "\n" ).formatted( fmt );
	}

	public TextBuilder( String initial ) {
		buf = Text.literal( initial + "\n" );
	}

	public TextBuilder() {
		buf = Text.literal( "" );
	}

	public void addLine( String string, Formatting fmt ) {
		add( string + "\n", fmt );
	}

	public void addLine( String string ) {
		add( string + "\n" );
	}

	public void add( String string, Formatting fmt ) {
		buf = buf.append( Text.literal( string ).formatted( fmt ) );
	}

	public void add( String string ) {
		buf = buf.append( Text.literal( string ).formatted( Formatting.RESET ) );
	}

	public void addParsed( String string ) {
		try {
			int lastIndex = 0;
			while ( string.contains( "<clr:" ) ) {
				// if there was text before the first tag, add it
				if ( lastIndex == 0 && string.contains( "<clr:" ) )
					buf.append( string.substring( 0, string.indexOf( "<clr:" ) ) );

				// get color
				int clrTagEnd = string.indexOf( ">" );
				lastIndex = clrTagEnd + 1;
				String clr = string.substring( string.indexOf( "<clr:" ) + 5, clrTagEnd );

				// get next clr tag (if exists)
				int nextClrTagStart = string.indexOf( "<clr:", lastIndex );
				nextClrTagStart = nextClrTagStart == -1 ? string.length() : nextClrTagStart;

				// create child
				buf.append(
					Text.literal( string.substring( clrTagEnd + 1, nextClrTagStart ) )
						.formatted( Formatting.byName( clr ) )
				);
				string = string.substring( nextClrTagStart );
			}
			if ( string.length() > 0 )
				buf.append( string );

			buf.append( "\n" );
		} catch ( Exception exc ) {
			exc.printStackTrace();
		}
	}

	public void resetStyle() {
		add( "" );
	}

	@Override
	public String toString() {
		return buf.toString();
	}

	public Text toText() {
		return buf;
	}
}
