package com.enderzombi102.gamemodes.util;

import com.mojang.brigadier.context.CommandContext;
import net.minecraft.block.AirBlock;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.TypeFilter;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.RaycastContext;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

public final class Util {
	private Util() {
	}

	public static MinecraftServer SERVER;
	public static final List<StatusEffect> LASTING_EFFECTS;
	public static final List<Block> SAFE_BLOCKS;
	public static final long ONE_SECOND = TimeUnit.SECONDS.toMillis( 1 );

	static {
		LASTING_EFFECTS = Registry.STATUS_EFFECT.getEntries()
				.stream()
				.filter( key -> !key.getKey().getValue().getPath().contains( "instant" ) )
				.map( Map.Entry::getValue )
				.toList();
		SAFE_BLOCKS = Registry.BLOCK.getEntries()
				.stream()
				.filter( key -> !( key.getValue() instanceof AirBlock ) )
				.map( Map.Entry::getValue )
				.toList();
	}

	public static void broadcastMessage( Text message ) {
		for ( var player : getPlayers() )
			player.sendMessage( message, false );
	}

	public static void broadcastMessage( String message ) {
		for ( var player : getPlayers() )
			player.sendMessage( Text.literal( message ), false );
	}

	public static BlockHitResult rayCast( ServerPlayerEntity player, int maxDistance ) {
		var cameraPos = player.getEyePos();
		var cameraRotation = player.getRotationVecClient();
		return player.getWorld().raycast(
			new RaycastContext(
				cameraPos,
				cameraPos.add(
					cameraRotation.x * maxDistance,
					cameraRotation.y * maxDistance,
					cameraRotation.z * maxDistance
				),
				RaycastContext.ShapeType.COLLIDER,
				RaycastContext.FluidHandling.NONE,
				player
			)
		);
	}

	public static ServerPlayerEntity getPlayer( UUID uuid ) {
		return SERVER.getPlayerManager().getPlayer( uuid );
	}

	public static List<ServerPlayerEntity> getPlayers() {
		return SERVER.getPlayerManager().getPlayerList();
	}

	public static void setMaxHealth( ServerPlayerEntity player, int health ) {
		var attribute = player.getAttributeInstance( EntityAttributes.GENERIC_MAX_HEALTH );
		assert attribute != null;
		attribute.setBaseValue( health );
		player.setHealth( health );
	}

	public static String format( String fmt, Object... objs ) {
		for ( Object obj : objs ) {
			fmt = fmt.replaceFirst( "\\{}", obj.toString() );
		}
		return fmt;
	}

	public static void killMonsters( ServerWorld world ) {
		killEntities(
			world,
			new ClassTypeFilter<>( PlayerEntity.class ),
			Objects::nonNull
		);
	}

	public static <T extends Entity> void killEntities( ServerWorld world, TypeFilter<Entity, T> filter, Predicate<Entity> predicate ) {
		world.getEntitiesByType( filter, predicate ).forEach( Entity::discard );
	}

	public static <T> T getArgument( CommandContext<ServerCommandSource> ctx, String name, Class<T> clazz, T defaultValue ) {
		try {
			return ctx.getArgument( name, clazz );
		} catch ( IllegalArgumentException ignored ) {
			return defaultValue;
		}
	}
}
